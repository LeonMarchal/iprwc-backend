package nl.hsleiden.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.security.Principal;

@Entity
@Table(name = "shopping_cart")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Cart.FIND_ALL",
                query = "SELECT * FROM shopping_cart;",
                resultClass = Cart.class
        ),
        @NamedNativeQuery(
                name = "Cart.FIND_BY_EMAIL",
                query = "SELECT * FROM shopping_cart WHERE email = :userEmail",
                resultClass = Cart.class
        ),
        @NamedNativeQuery(
                name = "Cart.FIND_BY_ID",
                query = "SELECT * FROM shopping_cart WHERE id = :Id",
                resultClass = Cart.class
        ),
        @NamedNativeQuery(
                name = "Cart.FIND_BY_EMAIL_AND_PRODUCT",
                query = "SELECT * FROM shopping_cart WHERE email = :userEmail AND product_id = :productId",
                resultClass = Cart.class
        )

})
public class Cart implements Principal{

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "email")
    private String email;

    @Column(name = "product_id")
    private long productId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @Override
    @JsonIgnore
    public String getName() {
        return email;
    }
}
