package nl.hsleiden.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.security.Principal;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "User.FIND_ALL",
                query = "SELECT * FROM users;",
                resultClass = User.class
        ),
        @NamedNativeQuery(
                name = "User.FIND_BY_EMAIL",
                query = "SELECT * FROM users WHERE email = :userEmail",
                resultClass = User.class
        )
})

public class User implements Principal {
    @Column(name = "full_name")
    private String fullName;

    @Column(name = "zip_code")
    private String postcode;

    @Column(name = "street_number")
    private String streetnumber;
    @Id
    @Column(name = "email")
    private String emailAddres;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String roles;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getStreetnumber() {
        return streetnumber;
    }

    public void setStreetnumber(String streetnumber) {
        this.streetnumber = streetnumber;
    }

    public String getEmailAddress() {
        return emailAddres;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddres = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    @JsonIgnore
    public String getName() {
        return fullName;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public boolean hasRole(String roleName) {
        if (roles != null) {
            if (roleName.equals(roleName)) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(User user) {
        return emailAddres.equals(user.getEmailAddress());
    }
}
