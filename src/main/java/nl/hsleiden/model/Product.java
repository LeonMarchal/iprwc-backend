package nl.hsleiden.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.security.Principal;

@Entity
@Table(name = "products")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Product.FIND_ALL",
                query = "SELECT * FROM products;",
                resultClass = Product.class
        ),
        @NamedNativeQuery(
                name = "Product.FIND_BY_ID",
                query = "SELECT * FROM products WHERE id = :productId",
                resultClass = Product.class
        )
})

public class Product implements Principal {
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String productName;

    @Column(name = "price")
    private String price;

    @Column(name = "picture")
    private String pictureLocation;

    @Column(name = "discription")
    private String discription;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPictureLocation() {
        return pictureLocation;
    }

    public void setPictureLocation(String pictureLocation) {
        this.pictureLocation = pictureLocation;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    @Override
    @JsonIgnore
    public String getName() {
        return productName;
    }
}
