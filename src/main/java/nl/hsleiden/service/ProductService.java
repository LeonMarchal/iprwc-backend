package nl.hsleiden.service;

import nl.hsleiden.dao.ProductDAO;
import nl.hsleiden.model.Product;
import nl.hsleiden.model.User;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

public class ProductService extends BaseService<Product> {
    private final ProductDAO dao;

    @Inject
    public ProductService(ProductDAO dao) {
        this.dao = dao;
    }

    public Collection<Product> getAllProducts() {
        return dao.getAllProducts();
    }

    public List<Product> get(long id) {
        return dao.findProductById(id);
    }

    public void add(Product product,User authenticator) {
        boolean excists = false;
        List<Product> products = dao.getAllProducts();
        for (Product u: products) {
            if (u.getId() == product.getId()){
                excists = true;
            }
        }
        if (!excists) {
            if (authenticator.getRoles().equals("ADMIN")) dao.updateOrCreateProduct(product);
        }
    }

    public void update(User authenticator, long id, Product product) {
        if (authenticator.hasRole("ADMIN")) dao.updateOrCreateProduct(product);
    }

    public void delete(Product product,User authenticator) {
        product = this.get(product.getId()).get(0);
        if (authenticator.getRoles().equals("ADMIN"))dao.delete(product);
    }
}
