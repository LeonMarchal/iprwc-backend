package nl.hsleiden.service;

import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

import nl.hsleiden.model.User;
import nl.hsleiden.dao.UserDAO;

@Singleton
public class UserService extends BaseService<User> {
    private final UserDAO dao;

    @Inject
    public UserService(UserDAO dao) {
        this.dao = dao;
    }

    public Collection<User> getAllUsers(User authenticator) {
        if (authenticator.getRoles().equals("ADMIN")) return dao.getAllUsers();
        else return null;
    }

    public User get(String email, User authenticator) {
        if (authenticator.getRoles().equals("ADMIN") || email.equals(authenticator.getEmailAddress()))
            return requireResult(dao.findUserByEmail(email).get(0));
        else return null;
    }

    public void add(User user) {
        boolean excists = false;
        List<User> users = dao.getAllUsers();
        for (User u : users) {
            if (u.getEmailAddress() == user.getEmailAddress()) {
                excists = true;
            }
        }
        if (!excists) {
            user.setRoles("GUEST");
            dao.updateOrCreateUser(user);
        }
    }

    public void update(User user, User authenticator, String email) {
        if (authenticator.getRoles().equals("ADMIN") || email.equals(authenticator.getEmailAddress()))
            dao.updateOrCreateUser(user);
    }

    public void delete(User authenticator, String email) {
        if (authenticator.getRoles().equals("ADMIN") || email.equals(authenticator.getEmailAddress())) {
            User user = this.get(email, authenticator);
            dao.delete(user);
        }
    }
}
