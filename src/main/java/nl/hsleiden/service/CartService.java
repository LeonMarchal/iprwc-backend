package nl.hsleiden.service;

import nl.hsleiden.dao.CartDAO;
import nl.hsleiden.model.Cart;
import nl.hsleiden.model.User;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.List;

@Singleton
public class CartService extends BaseService<Cart> {
    private final CartDAO dao;

    @Inject
    public CartService(CartDAO dao) {
        this.dao = dao;
    }

    public List<Cart> getAllCart(User authenticator) {
        if (authenticator.getRoles().equals("ADMIN")) return dao.getAllCarts();
        else return null;

    }

    public List<Cart> getByMail(String email, User authenticator) {
        if (authenticator.getRoles().equals("ADMIN") || email.equals(authenticator.getEmailAddress()))
            return dao.findCartByEmail(email);
        else return null;
    }


    public List<Cart> getByMailAndProduct(String email, long productId, User authenticator) {
        if (authenticator.getRoles().equals("ADMIN") || email.equals(authenticator.getEmailAddress()))
            return dao.findCartByEmailAndProduct(email, productId);
        else return null;
    }

    public void add(Cart cart, User authenticator) {
        if (authenticator.getRoles().equals("ADMIN") || cart.getEmail().equals(authenticator.getEmailAddress()))
            dao.updateOrCreateCart(cart);
    }

    public void update(Cart cart, User authenticator) {
        if (authenticator.getRoles().equals("ADMIN") || cart.getEmail().equals(authenticator.getEmailAddress()))
            dao.updateOrCreateCart(cart);
    }

    public void delete(Cart cart, long id, User authenticator) {
        cart = getByMailAndProduct(authenticator.getEmailAddress(), id, authenticator).get(0);
        if (authenticator.getRoles().equals("ADMIN") || cart.getEmail().equals(authenticator.getEmailAddress()))
            //Cart c = getSingle(id);
            dao.delete(cart);
    }

    public void deleteAll(User authenticator) {
        List<Cart> carts = getByMail(authenticator.getEmailAddress(), authenticator);
        for (Cart c : carts) {
            if (authenticator.getRoles().equals("ADMIN") || c.getEmail().equals(authenticator.getEmailAddress()))
                delete(c, c.getProductId(), authenticator);
        }
    }
}
