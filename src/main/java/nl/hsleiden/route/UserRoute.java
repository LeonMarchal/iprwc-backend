package nl.hsleiden.route;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;

import java.util.Collection;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import io.dropwizard.hibernate.UnitOfWork;
import nl.hsleiden.View;
import nl.hsleiden.model.User;
import nl.hsleiden.service.UserService;

@Singleton
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserRoute {
    private final UserService service;

    @Inject
    public UserRoute(UserService service) {
        this.service = service;
    }

    @GET
    @UnitOfWork
    public Collection<User> retrieveAllUsers(@Auth User authenticator) {
        return service.getAllUsers(authenticator);
    }

    @GET
    @UnitOfWork
    @Path("/{email}")
    public User retrieveByEmail(@PathParam("email") String email, @Auth User authenticator) {
        return service.get(email, authenticator);
    }

    @POST
    @UnitOfWork
    public void createUser(@Valid User user) {
        service.add(user);
    }

    @PUT
    @UnitOfWork
    @Path("/{email}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(@PathParam("email") String email, User user, @Auth User authenticator) {
        service.update(user, authenticator, email);
    }

    @DELETE
    @UnitOfWork
    @Path("/{email}")
    public void delete(@PathParam("email") String email, User user, @Auth User authenticator) {
        service.delete(authenticator, email);
    }

    @GET
    @UnitOfWork
    @Path("/me")
    public User authenticate(@Auth User authenticator) {
        return authenticator;
    }
}
