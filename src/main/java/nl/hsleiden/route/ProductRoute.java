package nl.hsleiden.route;

import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import nl.hsleiden.model.Product;
import nl.hsleiden.model.User;
import nl.hsleiden.service.ProductService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.List;

@Singleton
@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductRoute {
    private final ProductService service;

    @Inject
    public ProductRoute(ProductService service) {
        this.service = service;
    }

    @GET
    @UnitOfWork
    public Collection<Product> retrieveAllProducts() {
        return service.getAllProducts();
    }

    @GET
    @UnitOfWork
    @Path("/{id}")
    public List<Product> retrieveById(@PathParam("id") long id) {
        return service.get(id);
    }

    @POST
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public void createProduct(@Valid Product product, @Auth User authenticator) {
         service.add(product, authenticator);
    }

    @PUT
    @UnitOfWork
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(@PathParam("id") long id, @Auth User authenticator, Product product) {
        service.update(authenticator, id, product);
    }

    @DELETE
    @UnitOfWork
    @Path("/{id}")
    @RolesAllowed("ADMIN")
    public void delete(@PathParam("id") long id, Product product, @Auth User authenticator) {
        service.delete(product, authenticator);
    }
}
