package nl.hsleiden.route;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import nl.hsleiden.model.Cart;
import nl.hsleiden.model.User;
import nl.hsleiden.service.CartService;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.List;

@Singleton
@Path("/cart")
@Produces(MediaType.APPLICATION_JSON)
public class CartRoute {
    private final CartService service;

    @Inject
    public CartRoute(CartService service) {
        this.service = service;
    }

    @GET
    @UnitOfWork
    public Collection<Cart> retrieveAllCards(@Auth User authenticator) {
        return service.getAllCart(authenticator);
    }

    @GET
    @UnitOfWork
    @Path("/{email}")
    public List<Cart> retrieveByEmail(@PathParam("email") String email, @Auth User authenticator) {
            return service.getByMail(email, authenticator);

    }

    @GET
    @UnitOfWork
    @Path("/{email}/{id}")
    public List<Cart> retrieveByEmailAndProduct(@PathParam("email") String email, @PathParam("id") long id, @Auth User authenticator) {
            return service.getByMailAndProduct(email, id, authenticator);
    }

    @POST
    @UnitOfWork
    public void createCard(@Valid Cart cart, @Auth User authenticator) {
            service.add(cart,authenticator);
    }

    @PUT
    @UnitOfWork
    @Path("/{id}")
    public void update(@PathParam("id") long id, @Auth User authenticator, Cart cart) {
            service.update(cart,authenticator);
    }

    @DELETE
    @UnitOfWork
    @Path("/{id}")
    public void delete(@PathParam("id") long id, @Auth User authenticator, Cart cart) {
        service.delete(cart, id, authenticator);
    }

    @DELETE
    @UnitOfWork
    @Path("/mail")
    public void delete(@Auth User authenticator) {
        service.deleteAll(authenticator);
    }
}
