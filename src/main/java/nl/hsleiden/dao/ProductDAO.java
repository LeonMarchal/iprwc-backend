package nl.hsleiden.dao;

import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import nl.hsleiden.model.Product;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductDAO extends AbstractDAO<Product>{
    private final List<Product> products;
    private SessionFactory sessionFactory;

    @Inject
    public ProductDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
        products = new ArrayList<>();
    }

    public List<Product> getAllProducts() {
        return list(namedQuery("Product.FIND_ALL"));
    }

    public List<Product> findProductById(long id) {
        List<Product> result = products.stream()
                .filter(users -> users.getId() == id)
                .collect(Collectors.toList());
        if (!result.isEmpty()) return result;
        else return list(namedQuery("Product.FIND_BY_ID").setParameter("productId", id));

    }

    public void updateOrCreateProduct(Product product) {
        Product u = persist(product);
        sessionFactory.getCurrentSession().merge(product);
        products.add(u);
    }

    public void delete(Product product) {
        sessionFactory.getCurrentSession().delete(product);
        products.remove(product);
    }
}
