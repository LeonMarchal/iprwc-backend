package nl.hsleiden.dao;

import io.dropwizard.hibernate.AbstractDAO;
import nl.hsleiden.model.Cart;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class CartDAO extends AbstractDAO<Cart>{
    private final List<Cart> carts;
    private SessionFactory sessionFactory;

    @Inject
    public CartDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
        carts = new ArrayList<>();
    }

    public List<Cart> getAllCarts() {
        return list(namedQuery("Cart.FIND_ALL"));
    }

    public List<Cart> findCartByEmail(String email) {
        List<Cart> result = carts.stream()
                .filter(carts -> carts.getEmail() == email)
                .collect(Collectors.toList());
        if (!result.isEmpty()) return result;
        else return list(namedQuery("Cart.FIND_BY_EMAIL").setParameter("userEmail", email));
    }

    public List<Cart> findCartByEmailAndProduct(String email,long productId) {
        List<Cart> result = carts.stream()
                .filter(carts -> carts.getEmail() == email).filter(carts -> carts.getProductId() == productId)
                .collect(Collectors.toList());
        if (!result.isEmpty()) return result;
        else return list(namedQuery("Cart.FIND_BY_EMAIL_AND_PRODUCT").setParameter("userEmail", email).setParameter("productId", productId));
    }

    public List<Cart> findSingle(long id) {
        List<Cart> result = carts.stream()
                .filter(cart -> cart.getId() == id)
                .collect(Collectors.toList());
        if (!result.isEmpty()) return result;
        else return list(namedQuery("Cart.FIND_BY_ID").setParameter("Id", id));
    }

    public void updateOrCreateCart(Cart cart) {
        Cart u = persist(cart);
        sessionFactory.getCurrentSession().merge(cart);
        carts.add(u);
    }

    public void delete(Cart cart) {
        sessionFactory.getCurrentSession().delete(cart);
        carts.remove(cart);
    }
}
