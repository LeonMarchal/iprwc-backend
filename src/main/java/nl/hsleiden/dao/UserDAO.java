package nl.hsleiden.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;

import io.dropwizard.hibernate.AbstractDAO;
import nl.hsleiden.model.User;
import org.hibernate.SessionFactory;

@Singleton
public class UserDAO extends AbstractDAO<User> {
    private final List<User> users;
    private SessionFactory sessionFactory;

    @Inject
    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
        users = new ArrayList<>();
    }

    public List<User> getAllUsers() {
        return list(namedQuery("User.FIND_ALL"));
    }

    public List<User> findUserByEmail(String email) {
        List<User> result = users.stream()
                .filter(users -> users.getEmailAddress() == email)
                .collect(Collectors.toList());
        list(namedQuery("User.FIND_BY_EMAIL").setParameter("userEmail", email));
        if (!result.isEmpty()) return result;
        else return list(namedQuery("User.FIND_BY_EMAIL").setParameter("userEmail", email));

    }

    public void updateOrCreateUser(User user) {
        User u = persist(user);
        sessionFactory.getCurrentSession().merge(user);
        users.add(u);
    }

    public void delete(User user) {
        sessionFactory.getCurrentSession().delete(user);
        users.remove(user);
    }
}
