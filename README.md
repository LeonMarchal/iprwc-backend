# IprwcFrontend

https://gitlab.com/LeonMarchal/iprwc-frontend

# IprwcBackend

https://gitlab.com/LeonMarchal/iprwc-backend

## Docent Account

Guest account: 
Mail: docent@user.com
Wachtwoord: docentUser

## Admin Account

Admin account:
Mail: first@user.com
Wachtwoord: firstUser


## API Links

    GET     /api/ (nl.hsleiden.route.DefaultResource)
    GET     /api/cart (nl.hsleiden.route.CartRoute)
    POST    /api/cart (nl.hsleiden.route.CartRoute)
    GET     /api/cart/{email} (nl.hsleiden.route.CartRoute)
    GET     /api/cart/{email}/{id} (nl.hsleiden.route.CartRoute)
    DELETE  /api/cart/{id} (nl.hsleiden.route.CartRoute)
    PUT     /api/cart/{id} (nl.hsleiden.route.CartRoute)
    GET     /api/products (nl.hsleiden.route.ProductRoute)
    POST    /api/products (nl.hsleiden.route.ProductRoute)
    DELETE  /api/products/{id} (nl.hsleiden.route.ProductRoute)
    GET     /api/products/{id} (nl.hsleiden.route.ProductRoute)
    PUT     /api/products/{id} (nl.hsleiden.route.ProductRoute)
    GET     /api/users (nl.hsleiden.route.UserRoute)
    POST    /api/users (nl.hsleiden.route.UserRoute)
    GET     /api/users/me (nl.hsleiden.route.UserRoute)
    DELETE  /api/users/{email} (nl.hsleiden.route.UserRoute)
    GET     /api/users/{email} (nl.hsleiden.route.UserRoute)
    PUT     /api/users/{email} (nl.hsleiden.route.UserRoute)
